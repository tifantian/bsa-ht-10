const { GraphQLServer } = require('graphql-yoga');
const Query = require('./resolvers/Query');
const Mutation = require('./resolvers/Mutation');
const Message = require('./resolvers/Message');
const MessageAnswer = require('./resolvers/MessageAnswer');
const Subscription = require('./resolvers/Subscription');
const { prisma } = require('./generated/prisma-client');


const resolvers = {
  Query,
  Mutation,
  Subscription,
  Message,
  MessageAnswer
};

const server = new GraphQLServer({
  typeDefs: './server/src/schema.graphql',
  resolvers,
  context: request => ({
    ...request,
    prisma
  })
});

server.start(() => console.log('http://localhost:4000'));