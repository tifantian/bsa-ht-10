async function answer(parent, args, context) {
  const res = await context.prisma
    .message({
      id: parent.id
    })
    .answer();
  return res;
}

module.exports = {
  answer
};
