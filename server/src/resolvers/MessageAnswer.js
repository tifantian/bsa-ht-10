async function isAnswerToMessage(parent, args, context) {
  const res = await context.prisma
    .messageAnswer({
      id: parent.id
    })
    .isAnswerToMessage();
  return res;
}

module.exports = {
  isAnswerToMessage
};