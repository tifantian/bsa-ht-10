 async function postMessage(parent, args, context, info) {
  const { text } = args; 
  const countNumber = await context.prisma
    .messagesConnection()
    .aggregate()
    .count() + 1;
  

  return context.prisma.createMessage({
    text, countNumber
  });
}

async function like(parent, args, context) {
  const messageExists = await context.prisma.$exists.message({
    id: args.id
  }); 

  if (!messageExists) { 
    throw new Error(`Message with that ID does not exist`);
  }

  const where = { id: args.id }; 
  let likes = await context.prisma.message(where).likes();
  if (!likes) {
    likes = 0;
  }
  const data = { likes: likes + 1 };
  return context.prisma.updateMessage({ where, data });
}

async function dislike(parent, args, context) {
  const messageExists = await context.prisma.$exists.message({
    id: args.id
  });

  if (!messageExists) { 
    throw new Error(`Message with that ID does not exist`);
  }

  const where = { id: args.id }; 
  let dislikes = await context.prisma.message(where).dislikes();
  if (!dislikes) {
    dislikes = 0;
  }
  const data = { dislikes: dislikes + 1 };
  return context.prisma.updateMessage({ where, data });
}

function postNewAnswer(parent, args, context) {
  const { answer, id } = args;

  return context.prisma.createMessageAnswer({
    text: answer, 
    isAnswerToMessage: { connect: { id }}
  });
}

async function likeAnswer(parent, args, context) {
  const answerExists = await context.prisma.$exists.messageAnswer({
    id: args.id
  }); 

  if (!answerExists) { 
    throw new Error(`Message with that ID does not exist`);
  }

  const where = { id: args.id }; 
  let likes = await context.prisma.messageAnswer(where).likes();
  if (!likes) {
    likes = 0;
  }
  const data = { likes: likes + 1 };
  return context.prisma.updateMessageAnswer({ where, data });
}

async function dislikeAnswer(parent, args, context) {
  const answerExists = await context.prisma.$exists.messageAnswer({
    id: args.id
  });

  if (!answerExists) { 
    throw new Error(`answer with that ID does not exist`);
  }

  const where = { id: args.id }; 
  let dislikes = await context.prisma.messageAnswer(where).dislikes();
  if (!dislikes) {
    dislikes = 0;
  }
  const data = { dislikes: dislikes + 1 };
  return context.prisma.updateMessageAnswer({ where, data });
}

module.exports = {
  postMessage,
  like,
  dislike,
  postNewAnswer,
  likeAnswer,
  dislikeAnswer
}
