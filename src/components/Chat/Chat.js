import React from 'react';
import MessageList from '../MessageList/MessageList';
import MessageInput from '../MessageInput/MessageInput';
import './stylesChat.css';

class Chat extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      orderBy: 'createdAt_ASC',
      filter: "",
      skip: 0,
      skipField: "",
      first: null,
      lastMessageNumber: "",
      showError: false
    };
    this.orderBy = [
      'createdAt_ASC',
      'createdAt_DESC',
      'likes_DESC',
      'likes_ASC',
      'dislikes_DESC',
      'dislikes_ASC'
    ];
  }

  renderOptionsMenu() {
    const buttonsArray = this.orderBy.map(element => {
      return (
        <button
          key={element}
          onClick={() => this.updateOrderState(element)}
        >{element}
        </button>
      )
    });
    return buttonsArray;
  }

  updateOrderState(orderBy) {
    this.setState({ ...this.state, orderBy });
  }

  updateFilterState(filter) {
    this.setState({ ...this.state, filter });
  }

  updatePaginationState() {
    const { skipField, lastMessageNumber } = this.state;
    if (isNaN(skipField) || isNaN(lastMessageNumber)
      || lastMessageNumber < 1 || skipField < 1) {
      this.setState({ ...this.state, showError: true })
      return;
    }
    const skip = +skipField - 1;
    const first = +lastMessageNumber - skip;
    this.setState({ ...this.state, showError: false, skip, first });
  }

  render() {
    const {
      orderBy,
      filter,
      skip,
      first,
      skipField,
      lastMessageNumber,
      showError } = this.state;

    return (
      <div className="page-chat">
        <div className="chat-header">
          <div className="chat-name">
            Undefined chat
          </div>
          <div className="chat-sort-info">
            Select sorting type
          </div>
          <div className="chat-sort">
            {this.renderOptionsMenu()}
          </div>
          <div className="chat-filter">
            <input
              onChange={(event) => this.updateFilterState(event.target.value)}
              type="text"
              placeholder="Search messages"
              value={filter}
            />
          </div>
          <div className="chat-pagination">
            Show messages from
            <input
              type="text"
              value={skipField}
              placeholder="number value"
              onChange={(event) =>
                this.setState({ ...this.state, skipField: event.target.value, showError: false })
              }
            />
            to
            <input
              type="text"
              value={lastMessageNumber}
              placeholder="number value"
              onChange={(event) =>
                this.setState({ ...this.state, lastMessageNumber: event.target.value, showError: false })
              }
            />
            <button
              type="submit"
              onClick={() => this.updatePaginationState()}
            >
              Search
            </button>
          </div>
          {showError && <div className="warn-inputs">You can input only positive numbers</div>}
        </div>
        <MessageList
          orderBy={orderBy}
          filter={filter}
          skip={skip}
          first={first}
        />
        <MessageInput
          orderBy={orderBy}
          filter={filter}
          skip={skip}
          first={first}
        />
      </div>
    )
  }
}

export default Chat;