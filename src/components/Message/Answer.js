import React from 'react';
import { POST_DISLIKE_ANSWER, POST_LIKE_ANSWER } from '../queries';
import { Mutation } from 'react-apollo';

const Answer = ({ text, likes, dislikes, countNumber, id }) => {
  
  return (
    <div className="main-message answer">
      <div className="message-top">
        <div className="message-text">
          {text}
        </div>
        <div className="message-id">
          reply to {countNumber}
        </div>
      </div>
      <div className="message-bottom">

        <Mutation
          mutation={POST_LIKE_ANSWER}
          variables={{
            id
          }}
        >
          {likeMutation => (
            <div
              className="message-likes"
              onClick={event => {
                event.preventDefault();
                likeMutation();
              }}
            >
              {likes || 0} like
          </div>
          )}
        </Mutation>

        <Mutation
          mutation={POST_DISLIKE_ANSWER}
          variables={{
            id
          }}
        >
          {dislikeMutation => (
            <div
              className="message-dislikes"
              onClick={event => {
                event.preventDefault();
                dislikeMutation();
              }}
            >
              {dislikes || 0}  dislike
          </div>
          )}
        </Mutation>
      </div>
    </div>
  )
}

export default Answer;