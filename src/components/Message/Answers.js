import React from 'react';
import Answer from './Answer';

const Answers = ({ answerArray, answerToMessage }) => {

  return (
    <div className="answer-message-list">
      {answerArray.map(message => {
        const { text, likes, dislikes, id } = message;

        
        return (
          <Answer
            text={text}
            likes={likes}
            dislikes={dislikes}
            countNumber={answerToMessage}
            id={id}
            key={id}
          >

          </Answer>
        )
      })}
    </div>
  )
};

export default Answers;

