import  React, { useState } from 'react';
import { Mutation } from 'react-apollo';
import { POST_LIKE, POST_DISLIKE } from '../queries';
import ReplyModal from './ReplyModal';
import Answers from './Answers';

const Message = ({ text, likes, id, dislikes, countNumber, answer }) => {

  const [replyModeActive, setReplyModeActive] = useState(false);

  const replyToMessage = () => {
    setReplyModeActive(true);
  };

  const replyToMessageCompleted = () => {
    setReplyModeActive(false);
  };

  const getThreeDigitNumber = (countNumber) => {
    const stringCounter = String(countNumber);
    const split = stringCounter.split('');
    const zeroArray = Array(4 - split.length).join('0');
    return "#" + zeroArray + split.join('');
  };

  return (

    <div className="main-message">
      <div className="message-top">
        <div className="message-text">
          {text}
        </div>
        <div className="message-id">
          {getThreeDigitNumber(countNumber)}
        </div>
      </div>
      <div className="message-bottom">

        <Mutation
          mutation={POST_LIKE}
          variables={{
            id
          }}
        >
          {likeMutation => (
            <div
              className="message-likes"
              onClick={event => {
                event.preventDefault();
                likeMutation();
              }}
            >
              {likes || 0} like
          </div>
          )}
        </Mutation>

        <Mutation
          mutation={POST_DISLIKE}
          variables={{
            id
          }}
        >
          {dislikeMutation => (
            <div
              className="message-dislikes"
              onClick={event => {
                event.preventDefault();
                dislikeMutation();
              }}
            >
              {dislikes || 0}  dislike
          </div>
          )}
        </Mutation>

        <div
          className="message-reply"
          onClick={() => replyToMessage()}
        >
          Reply
        </div>

        {replyModeActive
          && (
            <ReplyModal
              exitModal={replyToMessageCompleted}
              replyToMessageWithId={id}
              threeDigitId={getThreeDigitNumber(countNumber)}
            >
            </ReplyModal>
          )
        }

      </div>
      { (answer && answer.length !== 0) 
        ? (
            <Answers
              answerArray={answer}
              answerToMessage={getThreeDigitNumber(countNumber)}
            >
            </Answers>
          ) : null   
      }
    </div>





  );
};

export default Message;