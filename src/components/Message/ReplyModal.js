import React, { useState } from 'react';
import { Mutation } from 'react-apollo';
import { POST_ANSWER } from '../queries';

const ReplyModal = ({ exitModal, replyToMessageWithId, threeDigitId }) => {

  const [text, setText] = useState('');

  const onChange = (event) => {
    setText(event.target.value);
  }

  const getDefaultState = () => {
    setText('');
  }

  return (
    <div className="message-modal-page">
      <div className="message-modal">
        <div className="modal-top">
          <div className="modal-name">
            Input reply to Message {threeDigitId}
          </div>
          <div className="modal-top-line">

          </div>
          <input className="modal-message-reply"
            type="text"
            value={text}
            onChange={onChange}
            placeholder="Enter message text..."
          />
        </div>
        <div className="modal-buttons">
          <Mutation
            mutation={POST_ANSWER}
            variables={{
              id: replyToMessageWithId,
              answer: text
            }}
          >
            {postNewAnswerMutation => (
              <button className="modal-edit"
                onClick={event => {
                  event.preventDefault();
                  if (!text) return;
                  postNewAnswerMutation();
                  getDefaultState();
                  exitModal();
                }}
              >
                Send
              </button>
            )}
          </Mutation>
          <button className="modal-cancel"
            onClick={exitModal}
          >
            Cancel
            </button>
        </div>

      </div>
    </div>
  )
};

export default ReplyModal;