import React, { useState } from 'react';
import { Mutation } from 'react-apollo';
import { GET_MESSAGES, POST_MESSAGE } from '../queries';

const MessageInput = ({ filter, skip, first, orderBy }) => {
  const [text, setText] = useState('');

  const onChange = (event) => {
    setText(event.target.value);
  } 

  const getDefaultState = () => {
    setText('');
  }
  
  const _updateStoreAfterAddingMessage = (store, newMessage) => {
    const data = store.readQuery({
      query: GET_MESSAGES,
      variables: {
        filter,
        skip,
        first,
        orderBy
    }
    });
    const { messageList, count, __typename } = data.messages;
    const newData = {
      messages: {
        count: count + 1,
        messageList: [...messageList, newMessage],
        __typename
      }
    }
    store.writeQuery({
      query: GET_MESSAGES,
      data: newData
    });
  };

  return (
    <Mutation
      mutation={POST_MESSAGE}
      update={(cache, { data: { postMessage } }) => {
        _updateStoreAfterAddingMessage(cache, postMessage);
      }}
      variables={{
        text
      }}
    >
      {messageMutation => (
        <div className="message-input">
          <input
            type="text"
            value={text}
            placeholder="Enter message text..."
            onChange={onChange}
          />
          <button
            onClick={event => {
              event.preventDefault();
              if (!text) return;
              messageMutation();
              getDefaultState();
            }}>
            Send
          </button>
        </div>
      )}
    </Mutation>
  );
};

export default MessageInput;