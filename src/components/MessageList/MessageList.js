import React from 'react';
import { Query } from 'react-apollo';
import Message from '../Message/Message';
import {
  GET_MESSAGES,
  NEW_MESSAGE_SUBSCRIPTION,
  NEW_ANSWER_SUBSCRIPTION
} from '../queries';

const MessageList = ({ orderBy, filter, skip, first }) => {

  const _subscribeToNewMessage = subscribeToMore => {
    subscribeToMore({
      document: NEW_MESSAGE_SUBSCRIPTION,
      updateQuery: (prev, { subscriptionData }) => {
        if (!subscriptionData.data) return prev;
        const { newMessage } = subscriptionData.data;
        const exists = prev.messages.messageList.find(({ id }) => id === newMessage.id);
        if (exists) return prev;

        return {
          ...prev, messages: {
            messageList: [...prev.messages.messageList, newMessage],
            count: prev.messages.messageList.length + 1,
            __typename: prev.messages.__typename
          }
        };
      }
    });
  };

  const _subscribeToNewAnswer = subscribeToMore => {
    subscribeToMore({
      document: NEW_ANSWER_SUBSCRIPTION,
      updateQuery: (prev, { subscriptionData }) => {
        if (!subscriptionData.data) return prev;
        const { newAnswer } = subscriptionData.data;
        const newMessageList = prev.messages.messageList.map(message => {
          if (message.id !== newAnswer.isAnswerToMessage.id) return message;
          message.answer.map(ans => {
            if (ans.id === newAnswer.id) return ans;
            return ans;
          });
          const index = message.answer.findIndex(ans => ans.id === newAnswer.id);
          if (index === -1) {
            return { ...message, answer: [...message.answer, newAnswer] };
          } else {
            const newMessage = { ...message };
            newMessage.answer[index] = newAnswer;
            return newMessage;
          }
        });

        return {
          ...prev,
          messages: {
            messageList: newMessageList,
            count: newMessageList.length,
            __typename: prev.messages.__typename
          }
        };
      }
    });
  };

  return (
    <Query query={GET_MESSAGES} variables={{ orderBy, filter, skip, first }} >
      {
        ({ loading, error, data, subscribeToMore }) => {
          if (loading) return (
            <div className="message-list-load">
              <div className="loader">Loading...</div>
            </div>
          );
          if (error) return (
            <div className="message-list-load">
              <div className="loader">No messages. Bad request</div>
            </div>
          );
          if (data.messages.messageList.length === 0) {
            return (
              <div className="message-list-load">
                <div className="loader">No messages</div>
              </div>
            )
          }

          _subscribeToNewMessage(subscribeToMore);
          _subscribeToNewAnswer(subscribeToMore);

          const { messageList, count } = data.messages;
          return (
            <div className="message-list">
              <div className="message-list-header">
                {messageList.length} messages :D
              </div>
              {messageList.map((message) => {
                const { text, likes, id, dislikes, countNumber, answer } = message;

                return (
                  <Message
                    text={text}
                    likes={likes}
                    dislikes={dislikes}
                    key={id}
                    id={id}
                    countNumber={countNumber}
                    answer={answer}
                  />
                )
              })}
            </div>
          )
        }
      }
    </Query>
  );
};

export default MessageList;