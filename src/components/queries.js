import gql from "graphql-tag";

export const GET_MESSAGES = gql`
  query messageQuery($filter: String, $skip: Int, $first: Int, $orderBy: MessagesOrderBy) {
    messages (filter: $filter, skip: $skip, first: $first, orderBy: $orderBy) {
    messageList {
      id 
      text 
      likes 
      dislikes 
      countNumber
      answer {
        id 
        text 
        likes 
        dislikes
      }
    }
    count
  }
  }
`;

export const POST_MESSAGE = gql`
  mutation messageMutation($text: String!) {
    postMessage(text: $text) {
      id
      text 
      likes 
      dislikes 
      countNumber
      answer {
        id
        text
        likes
        dislikes
      }
    }
  }
`;

export const NEW_MESSAGE_SUBSCRIPTION = gql`
  subscription {
    newMessage {
      id
      text 
      likes 
      dislikes 
      countNumber 
      answer {
        id
        text
        likes
        dislikes
        isAnswerToMessage {
          id
        }
      }
    }
  }
`;

export const POST_LIKE = gql`
  mutation likeMutation($id: ID!) {
    like(id: $id) {
      likes    
    }
  }
`;

export const POST_DISLIKE = gql`
  mutation dislikeMutation($id: ID!) {
    dislike(id: $id) {
      dislikes    
    }
  }
`;

export const POST_ANSWER = gql`
  mutation postNewAnswerMutation($answer: String! $id: ID!) {
    postNewAnswer(answer: $answer, id: $id) {
      id
      text
      likes 
      dislikes 
      isAnswerToMessage {
        id
      }
    }
  }
`;

export const POST_LIKE_ANSWER = gql`
  mutation likeAnswerMutation($id: ID!) {
    likeAnswer(id: $id) {
      likes    
    }
  }
`;

export const POST_DISLIKE_ANSWER = gql`
  mutation dislikeAnswerMutation($id: ID!) {
    dislikeAnswer(id: $id) {
      dislikes    
    }
  }
`;

export const NEW_ANSWER_SUBSCRIPTION = gql`
  subscription {
    newAnswer {
      id
      text
      likes
      dislikes
      isAnswerToMessage {
        id
      }
    }
  }
`;
